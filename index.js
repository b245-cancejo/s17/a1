/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function newUser() {
		let fullName = prompt("What is your Name? ");
		let age = prompt("How old are you? ");
		let address = prompt("Where do you live? ");
		
		alert("Thank you for your inputs.");
		console.log("Hello, " +fullName);
		console.log("You are " +age+ "years old.");
		console.log("You live in " +address);

	}
	newUser();


/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function favoriteArtists(){
		console.log("1. The Beatles");
		console.log("2. Parokya ni Edgar");
		console.log("3. Rivermaya");
		console.log("4. Eraserheads");
		console.log("5. MYMP");
	}
	favoriteArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	let tomatoesRating1 = "97%";
	let tomatoesRating2 = "96%";
	let tomatoesRating3 = "91%";
	let tomatoesRating4 = "93%";
	let tomatoesRating5 = "96%";

	function moviesRating(){
		console.log("1. The Godfather Rotten Tomatoes Rating: "+tomatoesRating1);
		console.log("2. The Godfather, Part II Tomatoes Rating: "+tomatoesRating2);
		console.log("3. Shawshank Redemption Tomatoes Rating: "+tomatoesRating3);
		console.log("4. Shawshank Redemption Rotten Tomatoes Rating: "+tomatoesRating4);
		console.log("5. Psycho Rotten Tomatoes Rating: "+tomatoesRating5);
	}

	moviesRating();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

//printUsers();

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

//console.log(friend1);
//console.log(friend2);*/